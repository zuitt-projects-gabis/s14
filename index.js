/*

	2 ways to add comments in JS

	// or ctrl + / - create a single line comment.
	/* *(sapce)/

	ctrl + shift + / - create a multi line comment
	Multi line comments allow us to add multiple lines in a single comment
*/	 
console.log("Hello,World");
console.log("Eiji")
console.log("Burrito")

/*
	console.log() allows us to show/display data in our console. The console is a part of our browser
	with which we can use to display data.
*/

/*
	Statement and Syntax

	Statements are instructions/expressions we add to our script/program which will
	then be communicated to our computers. Our computers will then be able to interpret these
	instructions and perform the task accordingly.

	Example:
	console.log("Eiji");
	We are actually the computer using JS to:
		"Display/Log this data in the console."

	Most programming languanges end their statements in semicolon(;)

	Most programming laungaunges end their statment in semicolon(;). However, JS
	does not require a semi-colon

	correct:
	console.log(<data>)

	wrong:
	()console.log
		"This data console display"

*/
/*
	Variables

	Html - element containers for text and other elements.

	Js - containers of data\

	To create a variable, we use the let keyword and the assignment operator (=)

	Syntax:

	let variableName = "data";

*/

let name = "Mark Adrian Gabis";
//log the value of the variable in the consloe;
console.log(name);

//save numbers in variables:

let num = 5;
let num2 = 10

//log the values of the variables in the console:
console.log(num);
console.log(num2);

//you could also check/display the values of multiple variables;

console.log(name,num,num2);

//We cannot display the value of a variable that is not yet created/declared
//In fact, that will result in an error. (not defined)

//console.log(name2);

let myVariable;

console.log(myVariable)//result: undefined
/*
	Creating variables is actually 2 steps:

	1. Declaration/Creation of the variable with either the let or const keyword.

	2. Initialization is when we provide an initial value to our variables.

	Declaration		Initialization
	ex: let myVar = Initial Value

*/	
myVariable = "new value";
console.log(myVariable);

let bestFinalFantasy = "Final Fantasy X";
console.log(bestFinalFantasy)

//You can update the values of a variable declared using the let keyword
bestFinalFantasy = "Final Fantasy 7";
console.log(bestFinalFantasy);

//We cannot update with the let keyword
//We cannot create another variable with the same name.

//let bestFinalFantasy = "Final Fantasy 6"
//console.log(bestFinalFantasy);

/*
	Const

	const keyword allows us to create a variable like let, however, the const variable cannot be updated.
	Values in a const variable cannot be changed. You also cannot
	create a const variable without initialization.

*/

const pi = 3.1416
console.log(pi);

//variable declared with const cannot be updated.

//pi = 3.15;
//console.log(pi);

let name2 = "Edward Cullen";
let role = "Supervisor";

role = "Director";
const tin = "1233-1234"
console.log(name2,role,tin);

/* 

	Conventions in creating variable/constant names:
	
	To create a let variable, we use the let keyword, to create a const variable.
	we use the const keyword.

	variables declared with let, its value can be updated, variables declared
	with const, we cannot change the value.

	let variables and const variables are usually name in small caps. Because other JS keywords that start in capital letters.

	If you want to name your variables with multiple keyworads, we can use camelCase. camelCase is a convetion in writing variables by adding the first word in small caps and the following words starting with the capital letters.

	variable names define the values they contain. Name your variables appropriately.


*/	

let user = "09322866426";
let carBrand ="Despacito";

//Data Types

/* 

	In most programming languanges, data is differentiated into different types.

	There are data types wherein we need to use literals to create them:
	To create strings, we use string litterals like '' or ""
	To create objects, we can use objects literals like {}
	To create arrays, we can use array literals like []

*/

/*

	Strings

	Strings are a series of alphanumeric characters that create a word, a phrase or anything that is related to creating a text.

	Strings are NOT and should not be used for mathematical operations.

	Strings are created with string literals like single quotes ('') or double quotes ("")


*/

let country = "Philippines";
let province = "Metro Manila";

console.log(province,country);


/*
	You can actually combine strings into a single string with the use of the plus sign or addition operator (+).

	This process is called concatanation.
	
*/

let address = province + ", " + country;
console.log(address);

let city = "Manila";
let city2 = "Cophenagen";
let city3 = "Washingston D.C.";
let city4 = "Tokyo";
let city5 = "New York";
let country1 = "Philippines";
let country2 = "U.S.A";
let country3 = "South Korea";
let country4 = "Japan";

let capital1 = country1 + "," + city2;
console.log(capital1)


let number1 = 10;
let number2 = 6;

console.log(number1);
console.log(number2);

let sum1 = number1 + number2;//16
console.log(sum1);

let sum2 = 16 + 4;
console.log(sum2);//20

let numString = "30";
let numString2 = "50";
/* These are numeric strings, they are composed numeric characters but are considered as string because it is surrounded/created with double quote("")*/

//let sumString1 = numString1 + numString2;
//console.log(sumString1);
//When numeric strings are used with addition opperator(+), it will not add but only combine the strings, it will concatenate.

//Note: When a number/integer is added with a numberic string, it will result to concatenation.
//let sum2 = number1 + numString2;
//console.log(sum3);//1050


//Activiyu
/* 
let fav2 = "Burrito";
console.log(fav2);

let sum = 150 + 9;
console.log(sum);

let prod = 100 * 90;
console.log(prod);

let favResto = ["Mcdonalds", "FaveBurrito", "Jolibee", "Burger King", "Kope-Right" ];
console.log(favResto);

let IsActive = true;

let favChar = {

*/

function printName(name){
	console.log(`My name is ${name}`)
};

//console.log(name);

printName("Jake");

function displayNum(number){
	console.log(number)
};

displayNum(3000);
displayNum(3001);

function printName(smessage){
	console.log(smessage)
};
printName("Javascript is fun!");

// Multiple Parameters and Arguments
	function displayFullName(firstName, lastName, age){
		console.log(`${firstName} ${lastName} is ${age}`)
	};

	displayFullName("Adrian", "Gabis", 24);

// return keyword
	function createFullName(firstName, middleName, lastName){
		return `${firstName} ${middleName} ${lastName}`
		console.log("I will no longer run because the function's calue/ result has been returned.")
	};

	let fullName1 = createFullName("Tom", "Cruise", "Mapother");
	console.log(fullName1);
